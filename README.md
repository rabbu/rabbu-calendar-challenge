# Rabbu Calendar Challenge

This Rails app has a simple data model setup and seeded

A `listing` is a property for rent.  It has many `channels` available online where guests can book it (Airbnb, Bookingcom, etc).  Each channel has many `calendar_days`, each of these days has a status of `available`, `blocked` or `reservation`.  Blocked dates are times when the listing cannot be booked.  For instance if there is a reservation on one channel, the same dates on other channels are blocked (if two channels are booked at once this is known as a double booking... aka bad news).  All channels might be blocked if the listing is unavailable due to maintenance.

The challenge is to create a "master calendar" view for the listing.  For any given day, no matter how many channels a listing is on, the listing can only have one status.  It is either available, blocked, or reserved.  A master calendar reduces all channel calendar days to a single day, selecting the relevant day's status to display.  Assume that the calendar cannot be in invalid states, for instance with a reservation on two channels for the same day (double booking).

Your master calendar can be setup however you'd like within Rails.  Beyond showing the correct status for a given day, it should always be up to date with the most recent data from the various channels' calendar days.

The master calendar should be viewable in the browser.  A minimum setup of VueJS has been included and should be used to render the calendar.  It does not need to be pretty but it should look like a month by month calendar.  Feel free to use a ready made calendar component.