import Vue from 'vue';
import _ from 'lodash';

import TurbolinksAdapter from 'vue-turbolinks';
Vue.use(TurbolinksAdapter);

import HomeComponent from '../components/home.vue'

document.addEventListener('turbolinks:load', () => {
  startVue('home', HomeComponent)
})

export function startVue(name, component) {
  const node = document.querySelector(`#vue-app[vue-app=${name}]`);
  if (!node) {
    return;
  }
  const tagName = node.tagName || "div"
  const classList = node.classList.toString()
  // Read and pass props into the main component
  const props = {};
  _.entries(node.dataset).forEach(([key, value]) => {
    try {
      props[key] = JSON.parse(value);
    } catch (e) {
      props[key] = value;
    }
  });
  // Basic Root instance
  const instance = {
    name: "App",
    render(h) {
      return h(
        tagName,
        {
          attrs: {
            id: `app-${name}`,
            class: classList
          }
        },
        [h(component, { props })]
      );
    }
  };
  return new Vue(instance).$mount(node);
}