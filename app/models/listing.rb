# == Schema Information
#
# Table name: listings
#
#  id           :bigint           not null, primary key
#  display_name :string
#  created_at   :datetime         not null
#  updated_at   :datetime         not null
#
class Listing < ApplicationRecord
  has_many :channels, dependent: :destroy
end
