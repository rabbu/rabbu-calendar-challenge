# == Schema Information
#
# Table name: calendar_days
#
#  id         :bigint           not null, primary key
#  date       :date
#  status     :integer          default("available"), not null
#  created_at :datetime         not null
#  updated_at :datetime         not null
#  channel_id :bigint           not null
#
# Indexes
#
#  index_calendar_days_on_channel_id  (channel_id)
#
# Foreign Keys
#
#  fk_rails_...  (channel_id => channels.id)
#
class CalendarDay < ApplicationRecord
  belongs_to :channel

  enum status: [:available, :blocked, :reservation], _prefix: true
end
