# == Schema Information
#
# Table name: channels
#
#  id           :bigint           not null, primary key
#  display_name :string
#  created_at   :datetime         not null
#  updated_at   :datetime         not null
#  listing_id   :bigint           not null
#
# Indexes
#
#  index_channels_on_listing_id  (listing_id)
#
# Foreign Keys
#
#  fk_rails_...  (listing_id => listings.id)
#
class Channel < ApplicationRecord
  belongs_to :listing
  has_many :calendar_days, dependent: :destroy
end
