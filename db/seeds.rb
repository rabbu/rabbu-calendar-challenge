# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the bin/rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)

Listing.destroy_all

listing = Listing.create(display_name: 'NYC_NMooreSt14')

airbnb = listing.channels.create(display_name: 'Airbnb')
bookingcom = listing.channels.create(display_name: 'Bookingcom')
direct = listing.channels.create(display_name: 'Direct')

dates = (Date.today.beginning_of_month..6.months.from_now.end_of_month.to_date).to_a
chunk_dates = []

airbnb.calendar_days.create(dates.map{ |d| {date: d} })
bookingcom.calendar_days.create(dates.map{ |d| {date: d} })
direct.calendar_days.create(dates.map{ |d| {date: d} })

(0..dates.size).to_a.inject(0) do |memo, n|
  next_index = memo + rand(14) + 2
  chunk_dates << dates[memo..next_index]
  next_index
end

chunk_dates.compact.map do |range|
  channels = [airbnb, bookingcom, direct]

  if rand(2) == 0
    # booked
    booked_channel = channels.delete_at(rand(channels.length))
    booked_channel.calendar_days.where(date: range).update(status: :reservation)

    channels.each do |channel|
      channel.calendar_days.where(date: range).update(status: :blocked)
    end
  elsif rand(9) == 0
    # blocked
    channels.each do |channel|
      channel.calendar_days.where(date: range).update(status: :blocked)
    end
  end
end