class CreateChannels < ActiveRecord::Migration[6.1]
  def change
    create_table :channels do |t|
      t.string :display_name
      t.references :listing, null: false, foreign_key: true

      t.timestamps
    end
  end
end
