class CreateCalendarDays < ActiveRecord::Migration[6.1]
  def change
    create_table :calendar_days do |t|
      t.date :date
      t.integer :status, null: false, default: 0
      t.references :channel, null: false, foreign_key: true

      t.timestamps
    end
  end
end
